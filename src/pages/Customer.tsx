import { useParams } from "react-router-dom";

const Customer = () => {
  let { id } = useParams();
  const customerId = `Customer ${id}`;

  return (
    <div className="page customer">{customerId}</div>
  );
};

export default Customer;
