import { Outlet } from "react-router-dom";

const Customers = () => {
  return (
    <div className="page customers">
      <Outlet />
      <span>Customers</span>
    </div>
  );
};

export default Customers;
