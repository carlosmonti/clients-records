import { MantineProvider } from '@mantine/core';
import React from 'react';
import ReactDOM from "react-dom/client";
import {
  BrowserRouter,
  Navigate,
  Route,
  Routes,
} from "react-router-dom";

import './index.css';
import Customer from './pages/Customer';
import Customers from './pages/Customers';
import Home from './pages/Home';
import Login from './pages/Login';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <React.StrictMode>
    <BrowserRouter basename='/clients-records'>
      <MantineProvider theme={{ fontFamily: "-apple-system, 'Helvetica Neue', sans-serif" }} withGlobalStyles withNormalizeCSS>
        <Routes>
          <Route path="/">
            <Route index element={<Home />} />
            <Route path="login" element={<Login />} />
            <Route path="customer/:id" element={<Customer />} />
            <Route path="customers" element={<Customers />} />
          </Route>
          <Route
            path="*"
            element={<Navigate to="/" replace />}
            />
        </Routes>
      </MantineProvider>
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
