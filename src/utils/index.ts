import { useEffect, useState } from 'react';
import userDataMock from '../../mocks/clients.response.ok.json';

const URL = 'https://randomuser.me/api/?inc=gender,name,location,email,dob,phone,cell&results=20&seed=clients-records';

// Create uuid
export const uuid = () => {
  // @ts-ignore-next-line
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    ((c ^ crypto.getRandomValues(new Uint8Array(1))[0]) & (15 >> c / 4)).toString(16)
  );
}

// Fake promise with some delay (in milliseconds)
export const mockAPIDelay = (data: {}, delay?: number) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve(data);
    }, delay);
  });

// Get mocked data
export const getData = async (isMock: boolean) => {
  let result = [];

  try {
    if (isMock) {// @ts-ignore-next-line
      result = await mockAPIDelay(userDataMock);
    } else {// @ts-ignore-next-line
      result = await fetch(
        URL,
        { mode: 'cors' }
      )
        .then(
          (response) => {
            const data = response.json();// @ts-ignore-next-line
            data.map((each: object) => ({
              ...each,
              id: uuid()
            }));
          }
        );
    }
  } catch (error) {
    throw new Error(`Boom! ${error}`)
  }

  return result;
};

// Hook
export const useDebounce = (value: string|number|boolean|symbol, delay: number) => {
  // State and setters for debounced value
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(
    () => {
      // Update debounced value after delay
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);
      // Cancel the timeout if value changes (also on delay change or unmount)
      // This is how we prevent debounced value from updating if value is changed ...
      // .. within the delay period. Timeout gets cleared and restarted.
      return () => {
        clearTimeout(handler);
      };
    },
    [value, delay] // Only re-call effect if value or delay changes
  );
  return debouncedValue;
};
