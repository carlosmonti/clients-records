# Clients Records

The idea is creating an application for keeping a customers database and any work done for them.

## Installation

```bash
git clone git@gitlab.com:carlosmonti/clients-records.git

npm install

npm start
```

## Deployment

Will be deployed to [insert-fly-io-url](https://fly.io/)

Now it's temporarily deployed [on Gitlab](https://carlosmonti.gitlab.io/clients-records)

## Stuff used

* [Random user generator API](https://randomuser.me/documentation) ([postman](https://www.getpostman.com/collections/6f9331c8ca9362579f17))
* [Mantine UI](https://mantine.dev/)
* Node 14 (npm 6.4)
* [Fly.io](https://fly.io/)
